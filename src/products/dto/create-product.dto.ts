import { IsNotEmpty, MinLength, IsPositive, Min } from 'class-validator';

export class CreateProductDto {
  @MinLength(8)
  @IsNotEmpty()
  name: string;

  //@IsPositive()
  @MinLength(1)
  @IsNotEmpty()
  price: number;
}
